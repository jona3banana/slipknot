$(function() {
    var files = [
        "images/41Ng2gcVnoL.jpg",
        "images/220px-SlipknotAntennastoHell.jpg",
        "images/4166b3a5d7b2cc7fc8e9d310cdbf9195.jpg",
        "images/746984046e504c47cc16fea98fe2d647.jpg",
        "images/dead-memories-5544e3449ab27.jpg",
        "images/iowa.jpg",
        "images/R-4295469-1361007045-2161.jpeg.jpg",
        "images/R-6934831-1431011833-7490.jpeg.jpg",
        "images/Slipknot_-_Slipknot2.jpg",
        "images/slipknot-sicnesses.jpg"
    ];
    var img;
    var currentNum = 0;
    for (var i = 0; i < files.length; i++) {
        img = $('<img>').attr('src', files[i]).addClass('thumbnail');
        $("#thumbnails").append(img);
    }
    $("#screen").append(
        $("<img>").attr("src", files[0])
        );
    $(".thumbnail:first").addClass('current');
    $('.thumbnail').click(function() {
        $('#screen img').attr('src', $(this).attr('src'));
        currentNum = $(this).index();
        $(this).addClass("current").siblings().removeClass('current');
    });
    $("#prev").click(function() {
        currentNum--;
        if (currentNum < 0){
                currentNum = files.length -1;
        }
        $("#screen img").attr("src", files[currentNum]);
          $(".thumbnail").removeClass("current"); $(".thumbnail").eq(currentNum).addClass("current");
        });
    $("#next").click(function() {
        currentNum++;
        if (currentNum > files.length -1) {
            currentNum = 0;
        }
        $("#screen img").attr("src", files[currentNum]);
        $(".thumbnail").removeClass("current"); $(".thumbnail").eq(currentNum).addClass("current");
    });
    $('#button').hide();
    //scroll action
    $(window).scroll(function() {
        $('#pos').text($(this).scrollTop());
            if($(this).scrollTop() > 60) {
                $("#back-to-top").fadeIn();
            } else {
                $("#back-to-top").fadeOut();
            };
// 
            if($(this).scrollTop() > 1494) {
                if($(this).scrollLeft() > 643) {
                    $("#button").fadeIn();
             } else {
                $('#button').fadeOut();
             }
            }

        $('#posb').text($(this).scrollLeft());
    });
    //click function
        $("#button").click(function() {
            $("#where").text("!");
            $("#map a").attr("href", "map_after.html");
        })
        $("#button").mousedown(function() {
            $(this).addClass('pushed').mouseup(function() {
                $(this).removeClass();
            });
        })
        $('#a').click(function() {
                $('body').animate( {
                    scrollTop: 600
                }, 500);
                return false;
            });
        $('#b').click(function() {
            $('body, html').animate( {
                scrollTop: 1200
            }, 500);
            return false;
        });
         $('#c').click(function() {
            $('body, html').animate( {
                scrollTop: 1955
            }, 500);
            return false;
        });
        $('#back-to-top').click(function() {
            $('body').animate( {
                scrollTop: 0
            }, 500);
            return false;
        })
//    Live
        $("#carouselInner").css("width", 620*$("#carousel ul.column").length+"px");
        $("#carouselInner ul.column:last").prependTo("#carouselInner");
        $("#carouselInner").css("margin-left", "-620px" );

//        戻るボタン
        $("#carouselPrev").click(function() {
            $("#carouselNext, #carouselPrev").hide();
            $("#carouselInner").animate({
                "margin-left" : parseInt($("#carouselInner").css("margin-left")) + 620 + "px"
            }, "slow", "swing",
            function(){
                $("#carouselInner").css("margin-left", "-620px");
                $("#carouselInner ul.column:last").prependTo("#carouselInner");
                $("#carouselNext, #carouselPrev").show();
            });
        });
        //次へボタン
        $("#carouselNext").click(function() {
            $("#carouselNext, #carouselPrev").hide();
            $("#carouselInner").animate({
                "margin-left" : parseInt($("#carouselInner").css("margin-left")) -620+"px"
            }, "slow", "swing", function() {
                $("#carouselInner").css("margin-left", "-620px");
                $("#carouselInner ul.column:first").appendTo("#carouselInner");
                $("#carouselNext, #carouselPrev" ).show();
            })
        })
});
